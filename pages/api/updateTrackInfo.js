import { connectToDatabase } from '../../util/mongodb';

export default async (req, res) => {
  try {
    const { db } = await connectToDatabase();
    const newTrack = await db.collection('data').findOneAndUpdate(
      { trackNumber: req.query.trackNumber },
      {
        $set: {
          trackNumber: req.query.trackNumber,
          reciever: req.query.reciever,
          recieverAddress: req.query.recieverAddress,
          harakterGruza: req.query.harakterGruza,
          weight: req.query.weight,
          parcelStatus: req.query.parcelStatus,
          paymentStatus: req.query.paymentStatus,
          recievingDate: req.query.recievingDate,
          sender: req.query.sender,
          senderAddress: req.query.senderAddress,
          senderPhone: req.query.senderPhone,
          price: req.query.price,
        },
      }
    );
    res.status(200).json(newTrack);
    return;
  } catch (err) {
    res.status(400).json(err);
  }
};
