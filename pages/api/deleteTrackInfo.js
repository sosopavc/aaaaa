import { connectToDatabase } from '../../util/mongodb';

export default async (req, res) => {
  try {
    const { db } = await connectToDatabase();
    const deletedItem = await db
      .collection('data')
      .findOneAndDelete({ trackNumber: req.query.trackNumber });
    res.status(200).json(deletedItem);
  } catch (err) {
    res.status(400).json(err);
  }
};
