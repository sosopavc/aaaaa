import { connectToDatabase } from '../../util/mongodb';

export default async (req, res) => {
  try {
    const { db } = await connectToDatabase();
    const exists = await await db
      .collection('data')
      .findOne({ trackNumber: req.query.trackNumber });
    if (exists) {
      res.status(409).json('TRACKEXISTS');
      return;
    }
    const newTrack = await db.collection('data').insertOne({
      trackNumber: req.query.trackNumber,
      reciever: req.query.reciever,
      recieverAddress: req.query.recieverAddress,
      harakterGruza: req.query.harakterGruza,
      weight: req.query.weight,
      parcelStatus: req.query.parcelStatus,
      paymentStatus: req.query.paymentStatus,
      recievingDate: req.query.recievingDate,
      sender: req.query.sender,
      senderAddress: req.query.senderAddress,
      senderPhone: req.query.senderPhone,
      price: req.query.price,
    });
    res.status(201).json(newTrack.ops[0]);
    return;
  } catch (err) {
    res.status(400).json(err.message);
  }
};
