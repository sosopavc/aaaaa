import { connectToDatabase } from '../../util/mongodb';

export default async (req, res) => {
  try {
    const { db } = await connectToDatabase();
    const exists = await db
      .collection('data')
      .findOne({ trackNumber: req.query.trackNumber });
    if (exists) {
      res.status(200).json(exists);
    } else {
      res.status(404).send();
    }
  } catch (err) {
    res.status(400).json(err);
  }
};
