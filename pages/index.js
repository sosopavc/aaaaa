import Head from 'next/head';
import React, { useState, useEffect } from 'react';
import TrackInfo from '../components/TrackInfo';
import axios from 'axios';
import {
  Grid,
  Button,
  TextField,
  InputAdornment,
  Modal,
  Paper,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import AddTrack from '../components/AddTrack';

import SearchIcon from '@material-ui/icons/Search';
import PasswordPage from '../components/PasswordPage';

export default function Home({ panelPassword }) {
  const [loggedIn, setLoggedIn] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [modalOpened, setModalOpened] = useState(false);
  const handleModalClose = () => setModalOpened(false);
  const [tracksData, setTracksData] = useState([]);
  const [sortedData, setSortedData] = useState(tracksData);
  const addTrackHandler = () => {
    setModalOpened(true);
  };
  const setTracks = (tr) => {
    console.log(tr);
    setTracksData((tracksData) => [...tracksData, tr]);
  };
  const deleteTrack = (trackNumber) => {
    setTracksData((tracksData) =>
      tracksData.filter((t) => t.trackNumber !== trackNumber)
    );
  };
  useEffect(() => {
    if (localStorage.getItem('loggedIn') === 'true') {
      setLoggedIn(true);
    }
  }, []);
  useEffect(() => {
    setSortedData(
      tracksData.filter(
        (t) =>
          t.trackNumber.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.reciever.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.recieverAddress.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.harakterGruza.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.weight.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.parcelStatus.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.paymentStatus.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.recievingDate.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.sender.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.senderAddress.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.senderPhone.toLowerCase().includes(searchValue.toLowerCase()) ||
          t.price.toLowerCase().includes(searchValue.toLowerCase())
      )
    );
  }, [searchValue]);
  useEffect(() => {
    if (loggedIn) {
      localStorage.setItem('loggedIn', 'true');
      try {
        axios
          .get('https://aaaaa-ten.vercel.app/api/getAllTracks')
          .then((res) => setTracksData(res.data));
      } catch (error) {
        console.log(error);
      }
    }
  }, [loggedIn]);
  if (loggedIn) {
    return (
      <div className="container" style={{ padding: '0 1rem 0 1rem' }}>
        <Head>
          <title>Админ-панель</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Modal open={modalOpened} onClose={handleModalClose}>
          <Paper
            style={{
              width: '100%',
              maxWidth: '40vw',
              maxHeight: '100%',
              position: 'fixed',
              display: 'flex',
              padding: '2rem',
              justifyContent: 'center',
              alignItems: 'center',
              top: '50%',
              left: '0',
              transform: 'translate(70%, -50%)',
              overflowY: 'auto',
            }}
          >
            <AddTrack
              setTracks={setTracks}
              handleModalClose={handleModalClose}
            />
          </Paper>
        </Modal>
        <Grid container>
          <Grid
            item
            container
            style={{ margin: '1em 0 2em 0' }}
            spacing={2}
            justifyContent="center"
            alignItems="center"
          >
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                startIcon={<AddIcon />}
                onClick={addTrackHandler}
              >
                Добавить трек
              </Button>
            </Grid>
            <Grid item>
              <TextField
                variant="outlined"
                size="small"
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
          </Grid>

          {tracksData.length > 0 && (
            <Grid
              item
              container
              spacing={3}
              justifyContent="center"
              alignItems="center"
            >
              {searchValue === '' &&
                tracksData.map((t) => (
                  <Grid item key={t._id}>
                    <TrackInfo track={t} deleteTrack={deleteTrack} />
                  </Grid>
                ))}
              {searchValue !== '' &&
                sortedData.map((t) => (
                  <Grid item key={t._id}>
                    <TrackInfo track={t} deleteTrack={deleteTrack} />
                  </Grid>
                ))}
            </Grid>
          )}
        </Grid>
      </div>
    );
  } else {
    return (
      <PasswordPage
        pass={panelPassword}
        loggedIn={loggedIn}
        setLoggedIn={setLoggedIn}
      />
    );
  }
}

export async function getServerSideProps(context) {
  const panelPassword = 123456;
  return {
    props: { panelPassword },
  };
}
