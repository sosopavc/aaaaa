import React, { useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  CircularProgress,
  CardActions,
  InputAdornment,
  IconButton,
  TextField,
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';

export default function TrackInfo({ track, deleteTrack }) {
  const [editMode, setEditMode] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const [isSaving, setIsSaving] = useState(false);
  const [trackNumberValue, setTrackNumberValue] = useState(track.trackNumber);
  const [recieverValue, setRecieverValue] = useState(track.reciever);
  const [recieverAddressValue, setRecieverAddressValue] = useState(
    track.recieverAddress
  );
  const [harakterGruzaValue, setHarakterGruzaValue] = useState(
    track.harakterGruza
  );
  const [weightValue, setWeightValue] = useState(track.weight);
  const [parcelStatusValue, setParcelStatusValue] = useState(
    track.parcelStatus
  );
  const [paymentStatusValue, setPaymentStatusValue] = useState(
    track.paymentStatus
  );
  const [recievingDateValue, setRecievingDateValue] = useState(
    track.recievingDate
  );
  const [senderValue, setSenderValue] = useState(track.sender);
  const [senderAddressValue, setSenderAddressValue] = useState(
    track.senderAddress
  );
  const [senderPhoneValue, setSenderPhoneValue] = useState(track.senderPhone);
  const [priceValue, setPriceValue] = useState(track.price);
  const editTrackHandler = () => {
    setEditMode(true);
  };
  const deleteTrackHandler = () => {
    setIsDeleting(true);
    try {
      axios
        .post(
          `https://aaaaa-ten.vercel.app/api/deleteTrackInfo?trackNumber=${track.trackNumber}`
        )
        .then((res) => {
          if (res.status === 200) {
            setIsDeleting(false);
            deleteTrack(track.trackNumber);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };
  const saveTrackHandler = () => {
    setEditMode(false);
    setIsSaving(true);
    try {
      axios
        .post(
          `https://aaaaa-ten.vercel.app/api/updateTrackInfo?trackNumber=${track.trackNumber}&reciever=${recieverValue}&recieverAddress=${recieverAddressValue}&harakterGruza=${harakterGruzaValue}&weight=${weightValue}&parcelStatus=${parcelStatusValue}&paymentStatus=${paymentStatusValue}&recievingDate=${recievingDateValue}&sender=${senderValue}&senderAddress=${senderAddressValue}&senderPhone=${senderPhoneValue}&price=${priceValue}`
        )
        .then((res) => {
          if (res.status === 200) {
            setIsSaving(false);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <Card variant="outlined" style={{ maxWidth: '18em' }}>
      <CardContent>
        <TextField
          label="Номер накладной"
          value={trackNumberValue}
          fullWidth
          onChange={(e) => {
            setTrackNumberValue(e.target.value);
          }}
          disabled
        />
        <TextField
          label="ФИО получателя"
          fullWidth
          value={recieverValue}
          onChange={(e) => {
            setRecieverValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Адрес получателя"
          fullWidth
          value={recieverAddressValue}
          onChange={(e) => {
            setRecieverAddressValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Характер груза"
          fullWidth
          value={harakterGruzaValue}
          onChange={(e) => {
            setHarakterGruzaValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Вес груза"
          fullWidth
          value={weightValue}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">Кг.</InputAdornment>
            ),
          }}
          onChange={(e) => {
            setWeightValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Статус посылки"
          fullWidth
          value={parcelStatusValue}
          onChange={(e) => {
            setParcelStatusValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Статус платежа"
          fullWidth
          value={paymentStatusValue}
          onChange={(e) => {
            setPaymentStatusValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Дата получения"
          fullWidth
          value={recievingDateValue}
          onChange={(e) => {
            setRecievingDateValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="ФИО отправителя"
          fullWidth
          value={senderValue}
          onChange={(e) => {
            setSenderValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Адрес отправителя"
          value={senderAddressValue}
          fullWidth
          onChange={(e) => {
            setSenderAddressValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Телефон отправителя"
          value={senderPhoneValue}
          fullWidth
          onChange={(e) => {
            setSenderPhoneValue(e.target.value);
          }}
          disabled={!editMode}
        />
        <TextField
          label="Цена"
          value={priceValue}
          fullWidth
          InputProps={{
            startAdornment: <InputAdornment position="start">₽</InputAdornment>,
          }}
          onChange={(e) => {
            setPriceValue(e.target.value);
          }}
          disabled={!editMode}
        />
      </CardContent>
      <CardActions>
        {!editMode && (
          <IconButton onClick={editTrackHandler}>
            <EditIcon />
          </IconButton>
        )}
        {editMode && !isSaving && (
          <IconButton onClick={saveTrackHandler}>
            <SaveIcon />
          </IconButton>
        )}
        {editMode && isSaving && <CircularProgress />}
        {isDeleting && <CircularProgress />}
        {!isDeleting && (
          <IconButton onClick={deleteTrackHandler}>
            <DeleteIcon />
          </IconButton>
        )}
      </CardActions>
    </Card>
  );
}
