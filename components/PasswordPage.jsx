import React, { useState, useEffect } from 'react';
import { Container, Grid, TextField, Button } from '@material-ui/core';

export default function PasswordPage({ pass, loggedIn, setLoggedIn }) {
  const [inputPasssword, setInputPassword] = useState('');
  const [passwordError, setPasswordError] = useState(false);
  const passwordInputHandler = (e) => {
    if (passwordError) {
      setPasswordError(false);
    }
    setInputPassword(e.target.value);
  };
  const loginHandler = () => {
    if ('123456' === inputPasssword) {
      setLoggedIn(true);
    } else {
      setPasswordError(true);
      setInputPassword('');
    }
  };
  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      direction="column"
      style={{ width: '100vw', height: '100vh' }}
    >
      <Grid item>
        <TextField
          variant="outlined"
          label="Пароль"
          error={passwordError ? true : false}
          type="password"
          style={{ width: '100%' }}
          onChange={passwordInputHandler}
          value={inputPasssword}
        ></TextField>
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          color="primary"
          disableElevation
          onClick={loginHandler}
          style={{ marginTop: '20px' }}
        >
          Войти
        </Button>
      </Grid>
    </Grid>
  );
}
