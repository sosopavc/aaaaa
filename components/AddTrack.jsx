import React, { useState } from 'react';
import {
  Card,
  CardContent,
  CardActions,
  InputAdornment,
  Button,
  TextField,
} from '@material-ui/core';
import axios from 'axios';
import AddCircleIcon from '@material-ui/icons/AddCircle';

export default function AddTrack({ setTracks, handleModalClose }) {
  const [isAdding, setIsAdding] = useState(false);
  const [exists, setExists] = useState(false);
  const [trackNumberValue, setTrackNumberValue] = useState('');
  const [recieverValue, setRecieverValue] = useState('');
  const [recieverAddressValue, setRecieverAddressValue] = useState('');
  const [harakterGruzaValue, setHarakterGruzaValue] = useState('');
  const [weightValue, setWeightValue] = useState('');
  const [parcelStatusValue, setParcelStatusValue] = useState('');
  const [paymentStatusValue, setPaymentStatusValue] = useState('');
  const [recievingDateValue, setRecievingDateValue] = useState('');
  const [senderValue, setSenderValue] = useState('');
  const [senderAddressValue, setSenderAddressValue] = useState('');
  const [senderPhoneValue, setSenderPhoneValue] = useState('');
  const [priceValue, setPriceValue] = useState('');
  const addTrackHandler = () => {
    setIsAdding(true);
    try {
      axios
        .post(
          `https://aaaaa-ten.vercel.app/api/addTrackInfo?trackNumber=${trackNumberValue}&reciever=${recieverValue}&recieverAddress=${recieverAddressValue}&harakterGruza=${harakterGruzaValue}&weight=${weightValue}&parcelStatus=${parcelStatusValue}&paymentStatus=${paymentStatusValue}&recievingDate=${recievingDateValue}&sender=${senderValue}&senderAddress=${senderAddressValue}&senderPhone=${senderPhoneValue}&price=${priceValue}`
        )
        .then((res) => {
          if (res.status === 201) {
            setIsAdding(false);
            setTracks(res.data);
            handleModalClose();
          }
        })
        .catch((err) => {
          if (err.response.status === 409) {
            setExists(true);
            setTrackNumberValue('');
            setIsAdding(false);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <Card variant="outlined" style={{ maxWidth: '20em' }}>
      <CardContent>
        <TextField
          label="Номер накладной"
          value={trackNumberValue}
          fullWidth
          error={exists}
          onChange={(e) => {
            if (exists) {
              setExists(false);
            }
            setTrackNumberValue(e.target.value);
          }}
        />
        <TextField
          label="ФИО получателя"
          fullWidth
          value={recieverValue}
          onChange={(e) => {
            setRecieverValue(e.target.value);
          }}
        />
        <TextField
          label="Адрес получателя"
          fullWidth
          value={recieverAddressValue}
          onChange={(e) => {
            setRecieverAddressValue(e.target.value);
          }}
        />
        <TextField
          label="Характер груза"
          fullWidth
          value={harakterGruzaValue}
          onChange={(e) => {
            setHarakterGruzaValue(e.target.value);
          }}
        />
        <TextField
          label="Вес груза"
          fullWidth
          value={weightValue}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">Кг.</InputAdornment>
            ),
          }}
          onChange={(e) => {
            setWeightValue(e.target.value);
          }}
        />
        <TextField
          label="Статус посылки"
          fullWidth
          value={parcelStatusValue}
          onChange={(e) => {
            setParcelStatusValue(e.target.value);
          }}
        />
        <TextField
          label="Статус платежа"
          fullWidth
          value={paymentStatusValue}
          onChange={(e) => {
            setPaymentStatusValue(e.target.value);
          }}
        />
        <TextField
          label="Дата получения"
          fullWidth
          value={recievingDateValue}
          onChange={(e) => {
            setRecievingDateValue(e.target.value);
          }}
        />
        <TextField
          label="ФИО отправителя"
          fullWidth
          value={senderValue}
          onChange={(e) => {
            setSenderValue(e.target.value);
          }}
        />
        <TextField
          label="Адрес отправителя"
          value={senderAddressValue}
          fullWidth
          onChange={(e) => {
            setSenderAddressValue(e.target.value);
          }}
        />
        <TextField
          label="Телефон отправителя"
          value={senderPhoneValue}
          fullWidth
          onChange={(e) => {
            setSenderPhoneValue(e.target.value);
          }}
        />
        <TextField
          label="Цена"
          value={priceValue}
          fullWidth
          InputProps={{
            startAdornment: <InputAdornment position="start">₽</InputAdornment>,
          }}
          onChange={(e) => {
            setPriceValue(e.target.value);
          }}
        />
      </CardContent>
      <CardActions style={{ display: 'flex', justifyContent: 'center' }}>
        <Button
          variant="contained"
          color="primary"
          startIcon={<AddCircleIcon />}
          onClick={addTrackHandler}
          disabled={isAdding}
        >
          Добавить трек
        </Button>
      </CardActions>
    </Card>
  );
}
